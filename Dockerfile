FROM openjdk:8-alpine
MAINTAINER dex.re

ENV version 2017-06-09
ENV work_dir /corenlp/
RUN mkdir $work_dir
WORKDIR $work_dir

RUN apk --no-cache add wget unzip

RUN wget http://nlp.stanford.edu/software/stanford-corenlp-full-${version}.zip
RUN unzip stanford-corenlp-full-${version}.zip
RUN rm -f http://nlp.stanford.edu/software/stanford-corenlp-full-${version}.zip
RUN cd stanford-corenlp-full-${version}

WORKDIR $work_dir/stanford-corenlp-full-${version}/
EXPOSE 9000
CMD java -mx4g -cp "*" edu.stanford.nlp.pipeline.StanfordCoreNLPServer -port 9000 -timeout 15000
